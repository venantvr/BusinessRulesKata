﻿/*
 * Created by SharpDevelop.
 * User: Rénald
 * Date: 30/07/2016
 * Time: 12:10
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

namespace BusinessModel
{
    public enum Category
    {
        PhysicalProduct,
        Book,
        Membership,
        Upgrade,
        Video
    }
}