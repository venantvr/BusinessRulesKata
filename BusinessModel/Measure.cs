﻿/*
 * Created by SharpDevelop.
 * User: Rénald
 * Date: 30/07/2016
 * Time: 12:10
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;

namespace BusinessModel
{
    public sealed class Measure
    {
        public readonly Action<Command> Action;

        public Measure(Action<Command> action)
        {
            Action = action;
        }
    }
}