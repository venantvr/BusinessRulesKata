﻿/*
 * Created by SharpDevelop.
 * User: Rénald
 * Date: 30/07/2016
 * Time: 12:10
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Reflection;
using BusinessModel;
using Contracts;

namespace BusinessRulesEngine
{
    public class BusinessRulesEngine
    {
        [ImportMany(@"Contracts.IBusinessRule")]
        // ReSharper disable once InconsistentNaming
        private IBusinessRule[] _rules { get; set; }

        public ReadOnlyCollection<IBusinessRule> Rules => _rules.ToList().AsReadOnly();

        private readonly Product _product;

        public BusinessRulesEngine(Product product)
        {
            var catalog = new AggregateCatalog();
            var container = new CompositionContainer(catalog);
            var batch = new CompositionBatch();

            foreach (var file in Directory.EnumerateFiles(AppDomain.CurrentDomain.BaseDirectory, @"*.dll"))
            {
                catalog.Catalogs.Add(new AssemblyCatalog(Assembly.LoadFrom(file)));
            }

            batch.AddPart(this);

            container.Compose(batch);

            _product = product;
        }

        public List<Measure> Compute()
        {
            var measures = new List<Measure>();
            foreach (var rule in _rules)
            {
                if (rule.Match(_product))
                {
                    measures.Add(rule.Decision);
                }
            }
            return measures;
        }

        public void Apply(IEnumerable<Measure> measures)
        {
            foreach (var measure in measures)
            {
                measure.Action.Invoke(_product.Command);
            }
        }
    }
}