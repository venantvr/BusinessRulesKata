﻿/*
 * Created by SharpDevelop.
 * User: Rénald
 * Date: 30/07/2016
 * Time: 12:10
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using BusinessModel;

namespace BusinessRulesKata
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var product = new Product { Category = Category.PhysicalProduct };

            var bre = new BusinessRulesEngine.BusinessRulesEngine(product);

            var actions = bre.Compute();

            bre.Apply(actions);
        }
    }
}