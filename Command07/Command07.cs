﻿/*
 * Created by SharpDevelop.
 * User: Rénald
 * Date: 30/07/2016
 * Time: 12:10
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using BusinessModel;
using Contracts;

namespace BusinessRules
{
    [Export(typeof (IBusinessRule))]
    [Description("Si le paiement est pour un produit physique ou pour un livre, générer un paiement de commission à l'agent.")]
    public class Command07 : IBusinessRule
    {
        public bool Match(Product product)
        {
            return product.Category == Category.PhysicalProduct || product.Category == Category.Book;
        }

        public Measure Decision
        {
            get { return new Measure(c => Console.WriteLine(@"Générer un paiement de commission à l'agent.")); }
        }
    }
}