using System;
using System.Linq;
using BusinessModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestProject
{
    [TestClass]
    public class Test_Bre
    {
        [TestInitialize]
        public void Prepare()
        {
        }

        [TestMethod]
        public void Test_For_Physical_Products()
        {
            using (var consoleWriter = new ConsoleWriter())
            {
                Console.SetOut(consoleWriter);

                var product = new Product { Category = Category.PhysicalProduct };

                var bre = new BusinessRulesEngine.BusinessRulesEngine(product);

                var measures = bre.Compute();

                bre.Apply(measures);

                Assert.IsTrue(consoleWriter.Lines.Count == 2);
                Assert.IsTrue(consoleWriter.Lines.Any(l => l == @"G�n�rer un bordereau d'exp�dition pour l'exp�dition."));
                Assert.IsTrue(consoleWriter.Lines.Any(l => l == @"G�n�rer un paiement de commission � l'agent."));
            }
        }

        [TestCleanup]
        public void CleanUp()
        {
        }
    }
}